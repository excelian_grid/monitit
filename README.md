# MONIT IT #

This project is for the Nucleus Cluster Event Processing SNMP Solution Deployment and Configuration Scripts.

### What is this repository for? ###

* Ansible deployment scripts for SNMP event monitoring of a Nucleus Platform Symphony Cluster
* Ansible configuration scripts for SNMP event monitoring of a Nucleus Platform Symphony Cluster
* Documentation of the process of setting up event monitoring in a Nucleus Platform Symphony Cluster
* Contributors List
* Issues management

### User Guide ###

* Ansible Scripts Configuration
* Documentation
* SNMP Event Monitoring Deployment instructions
* Configuring deployed SNMP event monitoring solution

### SNMP Monitoring Documentation ###

Documentation is generated with ansible-docgen. 

* Install ansible-docgen

easy_install ansible-docgen

[ansible-docgen](https://github.com/starboarder2001/ansible-docgen)

### Contributors ###
[Grzegorz Walczak](mailto:greg.walczak@excelian.com)